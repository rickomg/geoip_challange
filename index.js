'use strict'

Promise = require('bluebird');
const express = require('express');
const app = express();
const co = Promise.coroutine;
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

app.get('/', function (req, res) {
    res.send('hello hillwah');
});

// controller
const IPController = require('./controller/ip_controller');

// model
const IP = require('./model/ip');

// model instantiation
let ip = new IP();

// controllers
let ipController = new IPController(ip);

// IPController
app.get('/ip/:ipAddr', co(ipController.getWithIP));

app.listen(3033, function () {
    console.log('Magic start here, with port 3033 ...')
});

// adding stuff for pull request
console.log('this is something');
