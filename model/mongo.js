'use strict'

const mongo = require('../lib/mongo');

class Mongo {

    constructor (collection) {
        this.COLLECTION = collection;
        this.db = mongo();
    }

    insert (data) {
        return this.load().then((db) => {
            return db.insert(data);
        });
    }

    findOne (param) {
        return this.load().then((db) => {
            return db.find(param).limit(1).next();
        });
    }

    load () {
        let _this = this;
        return this.db.then((db) => {
            return db.collection(_this.COLLECTION);
        });
    }
}

module.exports = Mongo;
