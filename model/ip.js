'use strict'

const Mongo = require('./mongo');

class IP extends Mongo {

    constructor () {
        super('IPData');
    }
}

module.exports = IP;
