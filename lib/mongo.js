'use strict'

const MongoClient = require('mongodb').MongoClient;

let url = 'mongodb://localhost:27017/hillwah';

let mongo;

module.exports = function() {
    if (!mongo) {
        mongo = MongoClient.connect(url);
    }

    return mongo;
}
