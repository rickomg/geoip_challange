'use strict'

const request = require('request');

let ip;
let freeURI = 'http://freegeoip.net/json/';

class IPController {

    constructor (IP) {
        ip = IP;
    }

    * getWithIP (req, res) {
        let params = req.params;
        let ipAddr = params.ipAddr;
        let rsData = {};

        console.log(params);

        let rs = yield ip.findOne({
            ip: ipAddr
        });

        if (rs) {
            rsData = {
                country: rs.country_name
            }
            res.json(rsData);
        } else {
            request(
                {
                    url: freeURI + ipAddr,
                    method: "GET"
                }, function (error, response, body) {
                    let statusCode = response.statusCode;
                    console.log("Status code:", statusCode);
                    if (error) {
                        console.log("error: ", error);
                    }

                    if (statusCode != 200) {
                        res.sendStatus(statusCode);
                    } else {
                        console.log('reponse headers: ', response.headers)
                        console.log('geoip body: ', body);

                        let ipData = JSON.parse(body);
                        rsData = {
                            country: ipData.country_name
                        };
                        try {
                            ip.insert(ipData);
                        } catch (e) {
                            console.log(e);
                        }
                        res.json(rsData);
                    }
                }
            )
        }
    }
}

module.exports = IPController;
